<?php

use Illuminate\Database\Seeder;

class MessagesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Models\Message::class, rand(10, 30))->create([
            'user_id' => rand(1, 10),
        ])->each(function($message){
            $message->recievers()->saveMany(
                factory(App\Models\MessageReciever::class, rand(3, 10))->make([
                    'user_id' => rand(1, 10),
                ])
            );
        });
    }
}
