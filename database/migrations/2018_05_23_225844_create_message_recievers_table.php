<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMessageRecieversTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('message_recievers', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('message_id')->unsigned();
            $table->integer('user_id')->nullable();
            $table->string('email');
            $table->tinyInteger('status')->default(0)
                    ->comment('0 = Failed; 1 = Recieved; 2 = Pending; 3 = Read; 4 = Trash;');
            $table->timestamp('sended_at')->nullable();
            $table->timestamps();

            $table->foreign('message_id')->on('messages')->references('id')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('message_recievers');
    }
}
