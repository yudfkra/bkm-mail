<?php

use Faker\Generator as Faker;

$factory->define(App\Models\MessageReciever::class, function (Faker $faker) {
    $status = $faker->randomElement(array_values(App\Models\MessageReciever::MSG_STATUS));
    return [
        'email' => $faker->safeEmail,
        'status' => $status,
        'sended_at' => function () use ($status, $faker) {
            if ($status !== App\Models\MessageReciever::MSG_STATUS['pending']) {
                return $faker->dateTime('now', 'Asia/Jakarta');
            }
            return null;
        }
    ];
});
