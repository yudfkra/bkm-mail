<?php

use Faker\Generator as Faker;

$factory->define(App\Models\Message::class, function (Faker $faker) {
    return [
        'sender_name' => $faker->name,
        'sender_email' =>  $faker->safeEmail,
        'subject' => $faker->sentence(6, true),
        'body' => $faker->text(500),
    ];
});
