<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Arr;

class MessageReciever extends Model
{
    protected $table = 'message_recievers';

    protected $primaryKey = 'id';

    protected $perPage = 25;

    protected $fillable = [
        'message_id', 'email', 'status', 'sended_at',
    ];

    protected $dates = ['created_at', 'updated_at', 'sended_at'];

    const MSG_STATUS = [
        'fail' => 0,
        'sended' => 1,
        'pending' => 2,
        'read' => 3,
        'trash' => 4,
    ];

    /**
     * Get the Messages 
     *
     * @param array|string $search
     * @param string $type
     * @return array
     */
    public static function getMsgStatus($search = [], $type = "key")
    {
        $_status = ($type == "key") ? static::MSG_STATUS : array_flip(static::MSG_STATUS);

        if (!empty($search)) {
            if (is_string($search)) {
                $search = [$search];
            }

            $arr = [];
            foreach ($search as $key => $value) {
                if (array_key_exists($value, $_status)) {
                    $arr[] = $_status[$value];
                }
            }
            return array_values($arr);
        }
        return $_status;
    }

    public function scopeAuthUser($query, $id)
    {
        return $query->where(function($q) use($id){
            $q->where('user_id', $id)->orWhereRaw('BINARY `email` = ?', [$id]);
        });
    }

    public function scopeOnlyInbox($query)
    {
        return $this->scopeOnlyStatus($query, static::getMsgStatus([
            "sended", "read"
        ]));
    }

    public function scopeOnlyStatus($query, array $status = [])
    {
        if (!empty($status)) {
            $query->whereIn('status', $status);
        }
        return $query;
    }

    public function getMessageTypeDisplayAttribute()
    {
        $status = static::MSG_STATUS;
        if ($this->attributes['status'] == $status['sended'] || $this->attributes['status'] == $status['read']) {
            return 'Inbox';
        }
        return 'Trash';
    }

    public function getIsUnreadAttribute()
    {
        return $this->attributes['status'] === static::MSG_STATUS['sended'];
    }

    public function message()
    {
        return $this->belongsTo(Message::class, 'message_id', 'id');
    }
}
