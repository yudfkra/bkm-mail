<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Message extends Model
{
    protected $table = 'messages';

    protected $primaryKey = 'id';

    protected $fillable = [
        'user_id', 'sender_name', 'sender_email', 'subject', 'body',
    ];

    public function scopeAuthUser($query, $id)
    {
        return $query->where(function ($q) use ($id) {
            $q->where('user_id', $id)->orWhereRaw('BINARY `sender_email` = ?', [$id]);
        });
    }

    public function scopeJoinRecievers($query)
    {
        return $query->join(\DB::raw('(SELECT message_id, GROUP_CONCAT(email SEPARATOR ", ") as all_recievers FROM message_recievers GROUP BY message_id) as recievers'), 'messages.id', '=', 'recievers.message_id')->select('messages.*', 'recievers.all_recievers');
    }

    public function recievers()
    {
        return $this->hasMany(MessageReciever::class, 'message_id');
    }

    public function recieverEmails()
    {
        return $this->recievers()->where('email', '!=', 'auth_user_email');
    }
}
