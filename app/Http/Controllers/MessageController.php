<?php

namespace App\Http\Controllers;

use App\Libraries\Broker;
use App\Models\Message;
use App\Models\MessageReciever;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Requests\ComposeMessage;
use Carbon\Carbon;

class MessageController extends Controller
{
    protected $hanldeAction = ['trash', 'destroy', 'unread'];

    public function index(Request $request)
    {
        return view('site.pages.dashboard');
    }

    public function showInbox(Request $request)
    {
        $messages = MessageReciever::authUser($request->auth_user['id'])->with([
                        'message:id,sender_name,sender_email,subject,body'
                    ])->orderBy('sended_at', 'DESC');

        $trash_action = 'trash';
        $params = [];

        if ($request->filled('type')) {
            $type = $request->input('type', 'inbox');

            if (!empty($types = explode(':', $type))) {
                $search = MessageReciever::getMsgStatus($types);
                $messages = $messages->onlyStatus($search);

                $trash_action = in_array(MessageReciever::MSG_STATUS['trash'], $search) ? 'destroy' : 'trash';
            } else {
                $messages = $messages->onlyInbox();
            }
            $params = ['type' => $type];
        }else{
            $messages = $messages->onlyInbox();
        }
        
        $data['params'] = $params;
        $data['trash_action'] = $trash_action;
        $data['inboxes'] = $messages->paginate()->appends($params);

        $data['title'] = trans('layout.menu.message.child.inbox');
        // return $data;
        return view('site.pages.message.index', $data);
    }

    public function showMessage(Request $request, $id)
    {
        $message = MessageReciever::with('message')->authUser($request->auth_user['id'])->find($id);
        if (!empty($message)) {
            $message->update([
                'status' => MessageReciever::MSG_STATUS['read'],
            ]);
        }
        $data['message'] = $message;

        $data['status'] = MessageReciever::MSG_STATUS;

        $data['title'] = trans('message_page.detail');
        return view('site.pages.message.detail', $data);
    }

    public function showSentItem(Request $request)
    {
        // $messages = MessageReciever::authUser($request->auth_user['id'])
        //     ->onlyInbox()->with([
        //         'message:id,sender_name,sender_email,subject,body'
        //     ])->orderBy('sended_at', 'DESC');
        $sentItems = Message::JoinRecievers()->AuthUser($request->auth_user['id'])->latest();

        $data['sentitems'] = $sentItems->paginate();

        $data['title'] = trans('layout.menu.message.child.sent');
        // return $data;
        return view('site.pages.message.sentitems', $data);
    }

    public function showDetailSentItem(Request $request, $id)
    {
        $sentItems = Message::with('recievers')->JoinRecievers()->AuthUser($request->auth_user['id'])->find($id);
        
        $data['message'] = $sentItems;
        $data['title'] = trans('message_page.detail');
        return view('site.pages.message.sentitems_detail', $data);
    }

    public function showComposeForm()
    {
        $data['title'] = trans('layout.menu.message.child.compose');
        return view('site.pages.message.compose', $data);
    }

    public function storeMessage(ComposeMessage $request)
    {
        DB::beginTransaction();
        try{
            $message = new Message($request->only('subject', 'body'));
            $message->fill([
                'user_id' => $request->auth_user['id'],
                'sender_name' => $request->auth_user['name'],
                'sender_email' => $request->auth_user['email'],
            ]);
            $message->save();

            $emails = [];
            $lists = explode(',', $request->input('recievers'));
            foreach ($lists as $value) {
                $emails[] = [
                    'email' => $value,
                    'status' => MessageReciever::MSG_STATUS['pending'],
                ];
            }
            $message->recievers()->createMany($emails);
            DB::commit();

            $request->session()->flash('flash_messages', ['success' => trans('messages.success.')]);
            return redirect()->route('sent-items');
        }catch(\Exception $e){
            DB::rollback();

            $request->session()->flash('flash_messages', ['errors' => trans('messages.errors.')]);
        }
        return redirect()->back()->withInput();
    }

    public function handleMessageAction(Request $request)
    {
        $this->validate($request, [
            'inboxes.*' => 'required',
            'action' => 'required|string|in:' . implode(',', $this->hanldeAction),
        ]);
        if (count($request->input('inboxes')) >= 1) {
            $executed = DB::transaction(function () use ($request) {
                $emails = MessageReciever::authUser($request->auth_user['id'])->whereIn('id', $request->input('inboxes'));

                switch ($request->input('action')) {
                    case 'trash':
                        return $emails->update([
                            'status' => MessageReciever::MSG_STATUS['trash']
                        ]);
                        break;
                    case 'unread':
                        return $emails->update([
                            'status' => MessageReciever::MSG_STATUS['read']
                        ]);
                        break;
                    case 'destroy':
                        return $emails->delete();
                        break;
                    default:
                        $request->session()->flash('flash_messages', [
                            'errors' => trans('messages.errors.inbox_action_notfound')
                        ]);
                        return false;
                        break;
                }
            });
            if ($executed) {
                $request->session()->flash('flash_messages', [
                    'success' => trans('messages.success.inbox.action.' . $request->input('action'))
                ]);
            }else{
                $request->session()->flash('flash_messages', [
                    'errors' => trans('messages.errors.inbox.action.' . $request->input('action'))
                ]);
            }
            return redirect()->route('inbox.index', [
                'type' => $request->input('action') == 'trash' ? 'trash' :  '',
            ]);
        }

        $request->session()->flash('flash_messages', [
            'errors' => trans('messages.errors.empty_selected_inbox')
        ]);
        return redirect()->back()->withInput();
    }
}
