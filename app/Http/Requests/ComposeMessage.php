<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Validator;

class ComposeMessage extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'recievers' => 'required|string|emails',
            'subject' => 'required|string',
            'body' => [
                'required',
                'string',
                function ($attribute, $value, $fail) {
                    if (empty(trim(strip_tags($value)))) {
                        return $fail(trans('messages.errors.empty_message_body', [
                            'attribute' => $attribute
                        ]));
                    }
                }
            ],
        ];
    }

    public function attributes()
    {
        return [
            'recievers' => trans('message_page.field.to'), 
            'subject' => trans('message_page.field.subject'),
            'body' => trans('message_page.field.body'),
        ];
    }
}
