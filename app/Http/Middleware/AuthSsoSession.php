<?php

namespace App\Http\Middleware;

use Closure;
use App\Libraries\Broker;

class AuthSsoSession
{
    /**
     * Undocumented variable
     *
     * @var \App\Libraries\Broker
     */
    protected $broker;

    /**
     * Undocumented function
     *
     * @param Broker $broker
     * @return void
     */
    public function __construct(Broker $broker)
    {
        $this->broker = $broker;
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, ...$roles)
    {
        $this->broker->attach(true);
        if ($this->broker->check()) {
            $request->merge([
                'auth_user' => $this->broker->user(),
            ]);
            return $next($request);  
        }
        return redirect()->away(
            $this->broker->getServerURL(config('bkm.uri.login'), ['follow' => true])
        );
    }
}
