<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\Validator;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);
        Validator::extend('emails', function ($attribute, $value, $parameters, $validator){
            $values = explode(',', $value);
            if (!empty($values)) {
                foreach ($values as $email) {
                    $fails = Validator::make(['email' => $email], [
                        'email' => 'required|email',
                    ])->fails();
                    if ($fails) {
                        return false;
                    }
                }
                return true;
            }
            return false;
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
