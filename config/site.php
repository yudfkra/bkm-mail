<?php

return [
    'flash-message' => [
        'warnings' => 'warning',
        'errors' => 'danger',
        'success' => 'success',
        'infos' => 'info',
    ],
];