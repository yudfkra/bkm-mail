<?php

return [
    'host_url' => env('BKM_HOST_URL', 'http://localhost:8000/server'),
    'broker_id' => env('BKM_BROKER_ID', 2655616),
    'broker_secret' => env('BKM_BROKER_SECRET', 'ce51ded6cb17416671f393c2edfae7b4c20f1dcb'),
    'uri' => [
        'profile' => 'profile',
        'login' => 'login',
        'logout' => 'logout',
    ]
];