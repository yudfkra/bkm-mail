@extends('site.layouts.app')
@section('content')
    @includeWhen(session('flash_messages'), 'site.layouts.feedback')
    <div class="mail-container">
        @component('site.layouts.components.mail-header') 
            @slot('title') {{ $title }} @endslot
            {{ Form::open(['route' => 'sent-items', 'method' => 'GET', 'class' => 'pull-right', 'style' => 'width: 200px; margin-top: 3px;']) }}
                <div class="form-group input-group-sm has-feedback no-margin">
                    <input id="keyword" type="text" name="keyword" placeholder="{{ trans('message_page.field.search') }}" class="form-control">
                    <span class="fa fa-search form-control-feedback" style="top: -1px"></span>
                </div>
            {{ Form::close() }}
        @endcomponent
        {{ Form::open([
            'method' => 'patch',
            'route' => 'inbox.action',
        ]) }}
            <ul class="mail-list">
                @forelse ($sentitems as $item)
                    <li class="mail-item">
                        <div class="m-chck">
                            <label class="px-single">
                                <input type="checkbox" name="inboxes[]" value="{{ $item->id }}" class="px" {{ in_array($item->id, old('items', [])) ? 'checked' : '' }}>
                                <span class="lbl"></span>
                            </label>
                        </div>
                        {{-- <div class="m-star"><a href="#"></a></div> --}}
                        <div class="m-from">
                            <a href="{{ route('sent-items.detail', ['id' => $item->id]) }}"><span style="text-overflow: ellipsis;">{{ $item->all_recievers }}</span></a>
                        </div>
                        <div class="m-subject">
                            <a href="{{ route('sent-items.detail', ['id' => $item->id]) }}">{{ $item->subject }}</a>
                        </div>
                        <div class="m-date">{{ $item->created_at->diffForHumans() }}</div>
                    </li>
                @empty
                    <li class="mail-item">
                        <div class="text-center">Tidak ada pesan.</div>
                    </li>
                @endforelse
            </ul>
        {{ Form::close() }}
    </div>
@endsection