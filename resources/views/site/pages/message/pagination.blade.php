@if ($paginator->hasPages())
    <div class="btn-toolbar pull-right" role="toolbar">
        <div class="btn-group">
            {{-- Previous Page Link --}}
            @if ($paginator->onFirstPage())
                <button type="button" class="btn" disabled>
                    <i class="fa fa-chevron-left"></i>
                </button>
            @else
                <a href="{{ $paginator->previousPageUrl() }}" class="btn" rel="prev"><i class="fa fa-chevron-left"></i></a>
            @endif

            {{-- Next Page Link --}}
            @if ($paginator->hasMorePages())
                <a href="{{ $paginator->nextPageUrl() }}" class="btn" rel="prev"><i class="fa fa-chevron-right"></i></a>
            @else
                <button type="button" class="btn" disabled>
                    <i class="fa fa-chevron-right"></i>
                </button>
            @endif
        </div>
    </div>
@endif