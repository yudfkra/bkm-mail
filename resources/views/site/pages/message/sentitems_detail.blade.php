@extends('site.layouts.app')
@section('content')
    <div class="mail-container">
        @includeWhen(session('flash_messages'), 'site.layouts.feedback')
        @component('site.layouts.components.mail-header') 
            @slot('title') {{ $message ? $message->subject : $title }} @endslot
        @endcomponent
        @if (!empty($message))
            <div class="mail-controls clearfix">
                <div class="btn-toolbar wide-btns pull-left" role="toolbar">
                    <div class="btn-group">
                        <a href="{{ url()->previous() !== url()->current() ? url()->previous() : route('sent-items') }}" class="btn"><i class="fa fa-chevron-left"></i></a>
                    </div>
                    {{-- <div class="btn-group">
                        {{ Form::open([
                            'method' => 'patch',
                            'route' => 'inbox.action'
                        ]) }}
                            <input type="hidden" name="inboxes[]" value="{{ $message->id }}">
                            <button type="submit" name="action" value="{{ $message->statys == $status['trash'] ? 'destroy' : 'trash' }}" class="btn"><i class="fa fa-trash-o"></i></button>
                        {{ Form::close() }}
                    </div> --}}
                </div>
            </div>
            <div class="mail-info">
                {{ $message->all_recievers }}
            </div>
            <div class="mail-info">
                <div class="from">
                    <div class="name">{{ $message->sender_name }}</div>
                    <div class="email">{{ $message->sender_email }}</div>
                </div>
                <div class="date">{{ $message->created_at }}</div>
            </div>
            <div class="mail-message-body" style="border-bottom: 1px solid #efefef;">
                {!! $message->body !!}
            </div>        
        @endif
    </div>
@endsection