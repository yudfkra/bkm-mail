@extends('site.layouts.app')
@section('content')
    @includeWhen(session('flash_messages'), 'site.layouts.feedback')
    <div class="mail-container">
        @component('site.layouts.components.mail-header') 
            @slot('title') {{ $title }} @endslot
            {{ Form::open(['class' => 'pull-right', 'style' => 'width: 200px; margin-top: 3px;']) }}
                <div class="form-group input-group-sm has-feedback no-margin">
                    <input id="keyword" type="text" name="keyword" placeholder="{{ trans('message_page.field.search') }}" class="form-control">
                    <span class="fa fa-search form-control-feedback" style="top: -1px"></span>
                </div>
            {{ Form::close() }}
        @endcomponent
        {{ Form::open([
            'method' => 'patch',
            'route' => 'inbox.action',
        ]) }}
            <div class="mail-controls clearfix">
                <div class="btn-toolbar wide-btns pull-left" role="toolbar">
                    <div class="btn-group">
                        <div class="btn-group">
                            <button type="button" class="btn dropdown-toggle" data-toggle="dropdown"><i class="fa fa-check-square-o"></i>&nbsp;<i class="fa fa-caret-down"></i></button>
                            <ul class="dropdown-menu" role="menu">
                                <li><a href="#">@lang('message_page.text.check.all')</a></li>
                                <li><a href="#">@lang('message_page.text.check.unread')</a></li>
                                <li class="divider"></li>
                                <li><a href="#">@lang('message_page.text.uncheck.all')</a></li>
                                <li><a href="#">@lang('message_page.text.uncheck.unread')</a></li>
                            </ul>
                        </div>
                        <a href="{{ route(Route::currentRouteName()) }}" class="btn"><i class="fa fa-repeat"></i></a>
                    </div>
                    <div class="btn-group">
                        <button name="action" value="{{ $trash_action }}" type="submit" class="btn"><i class="fa fa-trash-o"></i></button>
                    </div>
                </div>
                {!! $inboxes->links('site.pages.message.pagination') !!}
                <div class="pages pull-right">
                    {{ trans('message_page.pagination_status', [
                        'from' => $inboxes->currentPage() > 1 
                                ? ($inboxes->currentPage() - 1) * $inboxes->perPage() + 1
                                : 1,
                        'to' => $inboxes->currentPage() > 1 
                                ? (($inboxes->currentPage() - 1) * $inboxes->perPage()) + $inboxes->count() 
                                : $inboxes->count(),
                        'total' => $inboxes->total(),
                    ]) }}
                </div>
            </div>
            <ul class="mail-list">
                @forelse ($inboxes as $inbox)
                    <li class="mail-item {{ $inbox->is_unread ? 'unread' : '' }}">
                        <div class="m-chck">
                            <label class="px-single">
                                <input type="checkbox" name="inboxes[]" value="{{ $inbox->id }}" class="px {{ $inbox->is_unread ? 'unread' : 'read' }}" {{ in_array($inbox->id, old('inboxes', [])) ? 'checked' : '' }}>
                                <span class="lbl"></span>
                            </label>
                        </div>
                        {{-- <div class="m-star"><a href="#"></a></div> --}}
                        <div class="m-from"><a href="{{ route('inbox.detail', ['id' => $inbox->id]) }}">{{ $inbox->message->sender_name }}</a></div>
                        <div class="m-subject">
                            {!! $inbox->is_unread ? '<span class="label label-info">' . trans('message_page.new') . '</span>' : '' !!}
                            &nbsp;&nbsp;
                            <a href="{{ route('inbox.detail', ['id' => $inbox->id]) }}">{{ $inbox->message->subject }}</a>
                        </div>
                        <div class="m-date">{{ $inbox->sended_at->diffForHumans() }}</div>
                    </li>
                @empty
                    <li class="mail-item">
                        <div class="text-center">Tidak ada pesan.</div>
                    </li>
                @endforelse
            </ul>
        {{ Form::close() }}
    </div>
@endsection