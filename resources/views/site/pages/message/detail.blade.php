@extends('site.layouts.app')
@section('content')
    <div class="mail-container">
        @includeWhen(session('flash_messages'), 'site.layouts.feedback')
        @component('site.layouts.components.mail-header') 
            @slot('title') {{ $message ? $message->message->subject : $title }} @endslot
            @if ($message)
                <span class="label label-primary">{{ $message->message_type_display }}</span>
            @endif
        @endcomponent
        @if (!empty($message))
            <div class="mail-controls clearfix">
                <div class="btn-toolbar wide-btns pull-left" role="toolbar">
                    <div class="btn-group">
                        <a href="{{ url()->previous() !== url()->current() ? url()->previous() : route('inbox.index') }}" class="btn"><i class="fa fa-chevron-left"></i></a>
                    </div>
                    <div class="btn-group">
                        {{ Form::open([
                            'method' => 'patch',
                            'route' => 'inbox.action'
                        ]) }}
                            <input type="hidden" name="inboxes[]" value="{{ $message->id }}">
                            <button type="submit" name="action" value="{{ $message->statys == $status['trash'] ? 'destroy' : 'trash' }}" class="btn"><i class="fa fa-trash-o"></i></button>
                        {{ Form::close() }}
                    </div>
                </div>
                @if ($message->status == $status['sended'] || $message->status == $status['read'])
                    <div class="btn-toolbar pull-right" role="toolbar">
                        <div class="btn-group">
                            <button type="button" class="btn btn-reply"><i class="fa fa-mail-reply"></i> @lang('message_page.button.reply')</button>
                        </div>
                    </div>
                @endif
            </div>
            <div class="mail-info">
                <div class="from">
                    <div class="name">{{ $message->message->sender_name }}</div>
                    <div class="email">{{ $message->message->sender_email }}</div>
                </div>
                <div class="date">{{ $message->sended_at }}</div>
            </div>
            <div class="mail-message-body" style="border-bottom: 1px solid #efefef;">
                {!! $message->message->body !!}
            </div>
            <div class="message-details-reply" style="margin-top: 16px;">
                @if ($message->status == $status['sended'] || $message->status == $status['read'])
                    <div id="message-details-reply" style="margin-top: 10px; {{ count($errors) == 0 ? 'display: none;' : '' }}">
                        {{ Form::open([
                            'route' => 'compose.store',
                            'class' => 'form-horizontal',
                        ]) }}
                            <div class="row form-group {{ $errors->has('recievers') ? 'has-error simple' : '' }}">
                                <label class="col-sm-2 control-label" for="mail-recievers">@lang('message_page.field.to')</label>
                                <div class="col-sm-10 select2-primary">
                                    <input id="mail-recievers" class="form-control" name="recievers" value="{{ old('recievers', $message->message->sender_email) }}" placeholder="{{ trans('message_page.field.to') }}">
                                    {!! $errors->has("recievers") ? '<p class="help-block" data-id="mail-recievers">'.$errors->first('recievers').'</p>' : "" !!}
                                </div>
                            </div>
                            <div class="row form-group {{ $errors->has('subject') ? 'has-error simple' : '' }}">
                                <label class="col-sm-2 control-label" for="mail-subject">@lang('message_page.field.subject')</label>
                                <div class="col-sm-10">
                                    <input class="form-control" name="subject" id="mail-subject" value="{{ old('subject', trans('message_page.reply_format', ['subject' => $message->message->subject])) }}" placeholder="{{ trans('message_page.field.subject') }}">
                                    {!! $errors->has("subject") ? '<p class="help-block" data-id="mail-subject">'.$errors->first('subject').'</p>' : "" !!}
                                </div>
                            </div>
                            <div class="row form-group {{ $errors->has('body') ? 'has-error simple' : '' }}">
                                <div class="col-sm-offset-2 col-sm-10">
                                    <textarea class="form-control" rows="5" name="body" id="mail-body" placeholder="{{ trans('message_page.field.body') }}">{!! old('body') !!}</textarea>
                                    {!! $errors->has("body") ? '<p class="help-block" data-id="mail-body">'.$errors->first('body').'</p>' : "" !!}
                                </div>
                            </div>
                            <div class="row form-group">
                                <div class="col-sm-offset-2 col-sm-10">
                                    <button type="submit" class="btn btn-primary">@lang('message_page.button.send')</button>
                                </div>
                            </div>
                        {{ Form::close() }}
                    </div>
                @endif
            </div>
        @else
            
        @endif
    </div>
@endsection
@push('scripts')
    <script type="text/javascript">
        window.initPixel.push(function(){
            $('.btn-reply').click(function(){
                // console.log($(this));
                $('#message-details-reply').slideToggle();
            });

            $('#mail-recievers').select2({
                minimumInputLength: 3,
                tags:[],
                tokenSeparators: [",", " "]
            });

            $('#message-details-reply textarea').summernote({
                height: 200,
                tabsize: 2
            });
        });
    </script>
@endpush