@extends('site.layouts.app')
@section('content')
    <div class="mail-container">
        @component('site.layouts.components.mail-header')
            @slot('title') {{ $title }} @endslot
        @endcomponent
        @includeWhen(session('flash_messages'), 'site.layouts.feedback')
        {{ Form::open([
            'route' => 'compose.store',
            'class' => 'new-mail-form form-horizontal'
        ]) }}
            <div class="row form-group {{ $errors->has('recievers') ? 'has-error simple' : '' }}">
                <label class="col-sm-2 control-label" for="mail-recievers">@lang('message_page.field.to')</label>
                <div class="col-sm-10 select2-primary">
                    <input id="mail-recievers" class="form-control" name="recievers" value="{{ old('recievers') }}" placeholder="{{ trans('message_page.field.to') }}">
                    {!! $errors->has("recievers") ? '<p class="help-block" data-id="mail-recievers">'.$errors->first('recievers').'</p>' : "" !!}
                </div>
            </div>
            <div class="row form-group {{ $errors->has('subject') ? 'has-error simple' : '' }}">
                <label class="col-sm-2 control-label" for="mail-subject">@lang('message_page.field.subject')</label>
                <div class="col-sm-10">
                    <input class="form-control" name="subject" id="mail-subject" value="{{ old('subject') }}" placeholder="{{ trans('message_page.field.subject') }}">
                    {!! $errors->has("subject") ? '<p class="help-block" data-id="mail-subject">'.$errors->first('subject').'</p>' : "" !!}
                </div>
            </div>
            <div class="row form-group {{ $errors->has('body') ? 'has-error simple' : '' }}">
                <div class="col-sm-offset-2 col-sm-10">
                    <textarea class="form-control" rows="5" name="body" id="mail-body" placeholder="{{ trans('message_page.field.body') }}">{!! old('body') !!}</textarea>
                    {!! $errors->has("body") ? '<p class="help-block" data-id="mail-body">'.$errors->first('body').'</p>' : "" !!}
                </div>
            </div>
            <div class="row form-group">
                <div class="col-sm-offset-2 col-sm-10">
                    <button type="submit" class="btn btn-primary">@lang('message_page.button.send')</button>
                </div>
            </div>
        {{ Form::close() }}
    </div>
@endsection
@push('scripts')
    <script type="text/javascript">
        window.initPixel.push(function(){
            $('#mail-recievers').select2({
                minimumInputLength: 3,
                tags:[],
                tokenSeparators: [",", " "]
            });

            $("#mail-body").summernote({
                height: 250,
                tabsize: 2,
            });
        });
    </script>
@endpush