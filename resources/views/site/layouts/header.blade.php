<button type="button" id="main-menu-toggle"><i class="navbar-icon fa fa-bars icon"></i><span class="hide-menu-text">@lang('admin/layout.buttons.hide_menu')</span></button>
<div class="navbar-inner">
    <div class="navbar-header">
        <a href="{{ route('index') }}" class="navbar-brand">
            <div><img alt="{{ config('app.name') }}" src="{{ asset('assets/logo.png') }}"></div>
            {{ config('app.name') }}
        </a>
        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#main-navbar-collapse"><i class="navbar-icon fa fa-bars"></i></button>
    </div>
    <div id="main-navbar-collapse" class="collapse navbar-collapse main-navbar-collapse">
        <div>
            <ul class="nav navbar-nav"></ul>
            <div class="right clearfix">
                <ul class="nav navbar-nav pull-right right-navbar-nav">
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle user-menu" data-toggle="dropdown">
                            <span>{{ $broker_profile['name'] }}</span>
                        </a>
                        <ul class="dropdown-menu">
                            {{-- <li>
                                <a href="#"><i class="dropdown-icon fa fa-cog"></i>&nbsp;&nbsp;Settings</a>
                            </li> --}}
                            <li class="divider"></li>
                            <li>
                                <a href="{{ $broker_logout_url }}"><i class="dropdown-icon fa fa-power-off"></i>&nbsp;&nbsp;Log Out</a>
                            </li>
                        </ul>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>