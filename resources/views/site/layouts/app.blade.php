<!DOCTYPE html>
<!--[if IE 8]>         <html class="ie8" lang="{{ app()->getLocale() }}"> <![endif]-->
<!--[if IE 9]>         <html class="ie9 gt-ie8" lang="{{ app()->getLocale() }}"> <![endif]-->
<!--[if gt IE 9]><!-->
<html class="gt-ie8 gt-ie9 not-ie" lang="{{ app()->getLocale() }}">
<!--<![endif]-->
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no, minimum-scale=1.0, maximum-scale=1.0">
    <title>{{ trans_choice('layout.meta.title', !empty($title), ['appname' => config('app.name'), 'title' => $title ?? '']) }}</title>
    <!-- Open Sans font from Google CDN -->
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,400,600,700,300&subset=latin"
        rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="{{ asset('pixel-admin/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('pixel-admin/css/pixel-admin.min.css') }}">
    <link rel="stylesheet" href="{{ asset('pixel-admin/css/widgets.min.css') }}">
    <link rel="stylesheet" href="{{ asset('pixel-admin/css/pages.min.css') }}">
    <link rel="stylesheet" href="{{ asset('pixel-admin/css/themes.min.css') }}">
    <!--[if lt IE 9]>
        <script src="assets/javascripts/ie.min.js"></script>
    <![endif]-->
    @stack('styles')
</head>

<body class="theme-asphalt no-main-menu page-mail">
    <div id="main-wrapper">
        <div id="main-navbar" class="navbar navbar-inverse" role="navigation">
            @include('site.layouts.header')
        </div>

        {{-- <div id="main-menu" role="navigation">
            @include('site.layouts.sidebar')
        </div> --}}

        <div id="content-wrapper">
            @include('site.layouts.nav-messages')
            @yield('content')
        </div>
        @stack('html')
        <div id="main-menu-bg"></div>
    </div>
    <script src="{{ asset('pixel-admin/js/jquery.min.js') }}"></script>
    <script src="{{ asset('pixel-admin/js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('pixel-admin/js/pixel-admin.min.js') }}"></script>
    <script src="{{ asset('js/bkm-mail.js') }}"></script>
    <script type="text/javascript">
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
    </script>
    @stack('scripts')
    <script type="text/javascript">
        window.PixelAdmin.start(window.initPixel, {
            main_menu: {
                detect_active: false,
            }
        });
    </script>
</body>
</html>