<div class="mail-nav">
    <div class="compose-btn">
        <a href="{{ route('compose') }}" class="btn btn-primary btn-labeled btn-block"><i class="btn-label fa fa-pencil-square-o"></i>@lang('layout.menu.message.child.compose')</a>
    </div>
    <div class="navigation">
        <ul class="sections">
            @if (Route::currentRouteName() == 'compose')
                <li class="mail-select-folder active"><a href="#">@lang('layout.menu.message.select_folder')</a></li>
            @endif
            <li class="{{ (!Request::has('type') && Request::segment(1)  == 'inbox') ? 'active' : '' }}">
                <a href="{{ route('inbox.index') }}">
                    <i class="m-nav-icon fa fa-inbox"></i>
                    @lang('layout.menu.message.child.inbox')
                    <span class="label pull-right">{{ $count_unread }}</span>
                </a>
            </li>
            <li class="{{ Route::currentRouteName() == 'sent-items' || Request::segment(1) == 'sent-items' ? 'active' : '' }}">
                <a href="{{ route('sent-items') }}">
                    <i class="m-nav-icon fa fa-envelope"></i>
                    @lang('layout.menu.message.child.sent')
                </a>
            </li>
            <li class="{{ (Request::has('type') && Request::input('type') == 'trash') ? 'active' : '' }}">
                <a href="{{ route('inbox.index', ['type' => 'trash']) }}">
                    <i class="m-nav-icon fa fa-trash-o"></i>
                    @lang('layout.menu.message.child.trash')
                </a>
            </li>
        </ul>
    </div>
</div>