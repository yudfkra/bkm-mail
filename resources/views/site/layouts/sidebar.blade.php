<div id="main-menu-inner">
    <ul class="navigation">
        <li class="{{ empty(Request::segment(1)) ? 'active' : '' }}">
            <a href="{{ route('index') }}">
                <i class="menu-icon fa fa-dashboard"></i>
                <span class="mm-text">@lang('layout.menu.dashboard.title')</span>
            </a>
        </li>
        <li class="mm-dropdown {{ in_array(Request::segment(1), ['inbox', 'compose', 'sent']) ? 'active open' : '' }}">
            <a href="#">
                <i class="menu-icon fa fa-envelope"></i>
                <span class="mm-text">@lang('layout.menu.message.title')</span>
            </a>
            <ul>
                <li class="{{ Request::segment(1) == 'inbox' ? 'active' : '' }}">
                    <a href="{{ route('inbox.index') }}">
                        <i class="menu-icon fa fa-inbox"></i>
                        <span class="mm-text">@lang('layout.menu.message.child.inbox')</span>
                    </a>
                </li>
                <li class="{{ Request::segment(1) == 'compose' ? 'active' : '' }}">
                    <a href="{{ route('compose') }}">
                        <i class="menu-icon fa fa-pencil-square-o"></i>
                        <span class="mm-text">@lang('layout.menu.message.child.compose')</span>
                    </a>
                </li>
                <li class="{{ Request::segment(1) == 'sent' ? 'active' : '' }}">
                    <a href="{{ route('sent-items') }}">
                        <i class="menu-icon fa fa-mail-forward"></i>
                        <span class="mm-text">@lang('layout.menu.message.child.sent')</span>
                    </a>
                </li>
            </ul>
        </li>
    </ul>
</div>