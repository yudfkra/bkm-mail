<?php

return [
    'meta' => [
        'title' => '{0} :appname |{1} :title - :appname',
    ],
    'menu' => [
        'dashboard' => [
            'title' => 'Dashboard',
        ],
        'message' => [
            'title' => 'Message',
            'child' => [
                'inbox' => 'Inbox',
                'compose' => 'Compose',
                'sent' => 'Sent Items',
                'trash' => 'Trash',
            ],
            'select_folder' => 'Select Folder ...',
        ]
    ],
];