<?php

return [
    'pagination_status' => ':from - :to of :total', 
    'field' => [
        'search' => 'Search',
        'subject' => 'Subject',
        'to' => 'To',
        'body' => 'Message',
    ],
    'button' => [
        'send' => 'Send Email',
        'reply' => 'Reply',
    ],
    'text' => [
        'check' => [
            'all' => 'Centang Semua',
            'unread' => 'Centang semua yang sudah dibaca',
        ],
        'uncheck' => [
            'all' => 'Hapus semua centang',
            'unread' => 'Hapus semua centang yang sudah dibaca',
        ]
    ],
    'new' => 'Baru',
    'detail' => 'Detail Message',
    'reply_format' => 'Re: :subject',
];