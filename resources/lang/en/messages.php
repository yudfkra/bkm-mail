<?php

return [
    'errors' => [
        'email_not_valid' => ':attribute :email is not valid.',
        'empty_message_body' => ':Attribute cannot be empty.',
    ]
];