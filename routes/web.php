<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Route::group(['middleware' => 'auth.sso'], function () {
    Route::get('/', 'MessageController@index')->name('index');

    Route::group(['prefix' => 'inbox', 'as' => 'inbox.'], function () {
        Route::get('/', 'MessageController@showInbox')->name('index');
        Route::patch('action', 'MessageController@handleMessageAction')->name('action');

        Route::get('message/{message}', 'MessageController@showMessage')->name('detail');
    });

    Route::get('compose', 'MessageController@showComposeForm')->name('compose');
    Route::post('compose', 'MessageController@storeMessage')->name('compose.store');

    Route::get('sent-items/{id}', 'MessageController@showDetailSentItem')->name('sent-items.detail');
    Route::get('sent-items', 'MessageController@showSentItem')->name('sent-items');
});
