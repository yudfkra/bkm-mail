let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/assets/js/app.js', 'public/js')
   .sass('resources/assets/sass/app.scss', 'public/css');

mix.js('resources/assets/js/bkm-mail.js', 'public/js');

// pixel-admin
mix.copy(['resources/assets/pixel-admin/javascripts/bootstrap.min.js',
    'node_modules/jquery/dist/jquery.min.js',
    'node_modules/jquery/dist/jquery.slim.min.js'], 'public/pixel-admin/js')
    .copy('resources/assets/pixel-admin/stylesheets/bootstrap.min.css', 'public/pixel-admin/css/bootstrap.min.css');

mix.copy('resources/assets/pixel-admin/fonts', 'public/pixel-admin/fonts')
    .copy(['resources/assets/pixel-admin/javascripts/pixel-admin.min.js',
        'resources/assets/pixel-admin/javascripts/ie.min.js', 'resources/assets/pixel-admin/javascripts/jquery-ui-extras.min.js'], 'public/pixel-admin/js')
    .copy(['resources/assets/pixel-admin/stylesheets/pixel-admin.min.css',
        'resources/assets/pixel-admin/stylesheets/pages.min.css',
        'resources/assets/pixel-admin/stylesheets/widgets.min.css',
        'resources/assets/pixel-admin/stylesheets/themes.min.css'], 'public/pixel-admin/css');
